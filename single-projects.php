<?php
/**
 * The template for displaying all single posts from a specific custom post types.
 */

get_header(); ?>

	<?php while ( have_posts() ) : the_post(); ?>

		<main class="main_wrapper single-project" data-page="single-project">

			<!-- Header -->

			<header class="single_project_header" style="background-image: url(<?php the_field('project_header_image'); ?>)">
				<div class="single_project_header_overlay"></div>
				<?php if(get_field('project_video_embed')): ?>
					<div class="play_button_container">
						<button class="play_button">Play</button>
					</div>
				<?php endif; ?>
			</header>

			<!-- Video Popup -->

			<?php 
			$projectVideoType = get_field('project_video_type');
			// First check if there is a video to be played.
			if($projectVideoType == 'youtube' || $projectVideoType == 'vimeo'): ?>

				<div class="project_video_container">

					<div class="project_video">

						<?php if($projectVideoType == 'youtube'): ?>

							<div data-type="youtube" data-video-id="<?php the_field('project_video_embed'); ?>"></div>

						<?php elseif ($projectVideoType == 'vimeo'): ?> 

							<div data-type="vimeo" data-video-id="<?php the_field('project_video_embed'); ?>"></div>
						
						<?php endif; ?>

					</div>

					<span class="close_icon"></span>

				</div>

			<?php endif; ?>

			<!-- Project Content -->

			<div class="container">

				<!-- Title / Excerpt -->

				<div class="humbleriot_row project_title_excerpt">

					<div class="column_1_2 project_title">
						<h1><?php the_field('project_title'); ?></h1>
					</div>

					<div class="column_1_2">
						<?php the_field('project_excerpt'); ?>
					</div>

				</div>

				<!-- First Image Set -->

				<div class="humbleriot_row">

					<?php $images = get_field('project_images'); ?>

					<?php if( isset($images[0]) ): ?>
					   	<div class="column_1_2">
					   		<img src="<?php echo $images[0]['url']; ?>" />
					    </div>
					<?php endif; ?>

					<?php if( isset($images[1]) ): ?>
					   	<div class="column_1_2">
					   		<img src="<?php echo $images[1]['url']; ?>" />
					    </div>
					<?php endif; ?>

				</div>

				<!-- Content -->

				<div class="project_content">
					<?php the_field('project_content'); ?>
				</div>

				<!-- Second Image Set -->

				<div class="humbleriot_row">

					<?php if( isset($images[2]) ): ?>
					   	<div class="column_1_2">
					   		<img src="<?php echo $images[2]['url']; ?>" />
					    </div>
					<?php endif; ?>

					<?php if( isset($images[3]) ): ?>
					   	<div class="column_1_2">
					   		<img src="<?php echo $images[3]['url']; ?>" />
					    </div>
					<?php endif; ?>

				</div>

				<!-- Bottom Gradient -->

				<div class="bottom_gradient"></div>

		</main>

	<?php endwhile; ?>

<?php get_footer(); ?>