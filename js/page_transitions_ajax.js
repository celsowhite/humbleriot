(function($) {
	$(document).ready(function() {
		'use strict';

		/*=======================
		Save universal elements that remain consistent across ajax transitions
		=======================*/

		var ajaxContainer = $('.main_content');
		var siteHeader = $('.main_header');
		var pageOverlay = $('.page_opacity_overlay');
		var teal = '#57BFC2';

		/*=======================
		Helper Functions
		=======================*/

		// Get the last string of a full URL

		var lastUrlSection = function(url) {
			var initialURLNoLastSlash = url.substr(0, url.length - 1);
			var initialSlug = initialURLNoLastSlash.split('/').pop();
			return initialSlug;
		};

		// Change a slug into a title

		var slugToTitle = function(slug) {
			// Remove the dashes
			slug = slug.replace(/-/g, ' ');
			// Change the first letter of every word into upper case
			return slug.replace(/(?:^|\s)\w/g, function(match) {
				return match.toUpperCase();
			});
		};

		// Change current menu item

		var changeActiveMenuLink = function(pageSlug) {
			$('.main_navigation li.current-menu-item').removeClass(
				'current-menu-item'
			);
			if (pageSlug !== 'home') {
				$('.main_navigation li.' + pageSlug).addClass('current-menu-item');
			}
		};

		/*=======================
		Set Initial History
		=======================*/

		// Get the URL of the page

		var initialURL = window.location.href;

		var initialSlug;

		if (initialURL === wpSiteURL) {
			initialSlug = 'home';
		} else {
			initialSlug = lastUrlSection(initialURL);
		}

		var initialTitle = slugToTitle(initialSlug);

		window.history.replaceState(initialSlug, initialTitle, '');

		/*=======================
		Homepage Transition
		=======================*/

		var homeLeaving = function(pageSlug, pageURL, pageTitle) {
			// Save homepage variables for future transitioning

			var homeMainWrapper = $('.main_wrapper.home');
			var homePanels = $('.humbleriot_panels');
			var homeCenterPanel = $('.humbleriot_panels .center');
			var homeLeftPanel = $('.humbleriot_panels .left');
			var homeRightPanel = $('.humbleriot_panels .right');
			var homePanelContent = $('.humbleriot_panels .panel_content_container');

			// Smooth transition out the home content. Opening up the center black panel. Changing the
			// main wrapper background to white so it blends with the new ajax content.

			homeLeftPanel.velocity({ left: '30%' }, 'easeOutCubic', 1000);
			homeRightPanel.velocity({ left: '40%' }, 'easeOutCubic', 1000);
			homeCenterPanel.velocity({
				backgroundColor: '#000000',
				backgroundColorAlpha: 1,
			});
			homeCenterPanel.velocity(
				{ left: '0%', width: '100%' },
				1000,
				'easeInCubic',
				function() {
					// Ensure once closed up that the homepage styles behind the layer transition.

					homePanels.addClass('leaving');
					homeMainWrapper.css({ 'background-image': 'none' });

					// Fade out the text on the homepage then open up the panel again.

					homePanelContent.velocity(
						{ opacity: 0, marginTop: '100px' },
						{
							duration: 1000,
							delay: 500,
							complete: function() {
								if (pageSlug === 'projects' || pageSlug === 'people') {
									siteHeader.addClass('dark');
								} else {
									siteHeader.removeClass('dark');
								}
								homeCenterPanel.velocity({ height: '0%' }, 1000, function() {
									// Empty the current content on the page
									ajaxContainer.empty();
									// Load in the new pages content
									ajaxContainer.load(pageURL + ' .main_wrapper', function() {
										// Change the browser URL & document title
										window.history.pushState(pageSlug, pageTitle, pageURL);
										document.title = pageTitle;
										$.ajaxSetup({ cache: true });
										$.getScript(
											wpSiteURL +
												'/wp-content/themes/humbleriot/js/scripts.min.js'
										);
									});
								});
							},
						}
					);
				}
			);
		};

		/*=======================
		Projects Page Transition
		=======================*/

		var projectsLeaving = function(pageSlug, pageURL, pageTitle) {
			// Variables

			var projectsGridItems = $('.grid_item');
			var projectGridItemInner = $('.grid_item:nth-child(3n+1)');
			var projectGridItemMiddle = $('.grid_item:nth-child(3n)');
			var projectGridItemOuter = $('.grid_item:nth-child(3n+2)');

			// Set the page overlay color

			pageOverlay.css({ 'background-color': teal, opacity: '.5' });

			// Fade in the page overlay so we can see the current pages contents dissapear behind a veil

			pageOverlay.fadeIn('slow', function() {
				projectGridItemInner.velocity({ opacity: 0, marginTop: '100px' }, 1000);
				projectGridItemMiddle.velocity({ opacity: 0, marginTop: '50px' }, 1000);
				projectGridItemOuter.velocity(
					{ opacity: 0, marginTop: '200px' },
					1000,
					function() {
						pageOverlay.velocity(
							{ backgroundColor: '#FFF', opacity: 1 },
							500,
							function() {
								// Empty the current content on the page
								ajaxContainer.empty();
								// Load in the new pages content
								ajaxContainer.load(pageURL + ' .main_wrapper', function() {
									// Change the browser URL & document title
									window.history.pushState(pageSlug, pageTitle, pageURL);
									document.title = pageTitle;
									// Ensure window is at the top.
									window.scrollTo(0, 0);
									$.ajaxSetup({ cache: true });
									$.getScript(
										wpSiteURL +
											'/wp-content/themes/humbleriot/js/scripts.min.js'
									);
									pageOverlay.velocity(
										{ opacity: 0 },
										{
											duration: 1000,
											delay: 800,
											complete: function() {
												pageOverlay.css({ display: 'none', height: '100%' });
												if (pageSlug === 'projects' || pageSlug === 'people') {
													siteHeader.addClass('dark');
												} else {
													siteHeader.removeClass('dark');
												}
											},
										}
									);
								});
							}
						);
					}
				);
			});
		};

		/*=======================
		Single Project Page Transition
		=======================*/

		var singleProjectLeaving = function(pageSlug, pageURL, pageTitle) {
			// Variables

			var headerImage = $('.single_project_header');
			var playButton = $('.play_button');
			var projectContainer = $('.container');

			// Set the page overlay color for the elements transitioning out

			pageOverlay.css({ 'background-color': '#FFFFFF', opacity: '.5' });

			playButton.velocity({ opacity: 0 }, 1000);

			projectContainer.velocity(
				{ paddingTop: '100px', opacity: 0 },
				{
					duration: 1000,
					delay: 0,
					easing: 'easeInCubic',
					complete: function() {
						headerImage.velocity({ height: '0vh' }, 1000, function() {
							siteHeader.addClass('dark');
							pageOverlay.fadeIn('slow', function() {
								// Turn the page overlay to white so the ajax content can load behind it
								pageOverlay.velocity(
									{ backgroundColor: '#FFF', opacity: 1 },
									500,
									function() {
										// Empty the current content on the page
										ajaxContainer.empty();
										// Load in the new pages content
										ajaxContainer.load(pageURL + ' .main_wrapper', function() {
											// Change the browser URL & document title
											window.history.pushState(pageSlug, pageTitle, pageURL);
											document.title = pageTitle;
											$.ajaxSetup({ cache: true });
											$.getScript(
												wpSiteURL +
													'/wp-content/themes/humbleriot/js/scripts.min.js'
											);

											// Page header transition color scheme

											if (pageSlug === 'projects' || pageSlug === 'people') {
												siteHeader.addClass('dark');
											} else {
												siteHeader.removeClass('dark');
											}
											// Fade out page overlay to reveal page content
											pageOverlay.velocity(
												{ opacity: 0 },
												{
													duration: 1000,
													delay: 300,
													complete: function() {
														pageOverlay.css({
															display: 'none',
															height: '100%',
														});
													},
												}
											);
										});
									}
								);
							});
						});
					},
				}
			);
		};

		/*=======================
		People Page Transition
		=======================*/

		var peopleLeaving = function(pageSlug, pageURL, pageTitle) {
			// Variables

			var peopleBorder = $('.people_border');
			var peopleArtists = $('.people_artists');
			var peopleClients = $('.people_clients');
			var artistBlocksGroup1 = $(
				'.people_artists .column_1_3:nth-child(3n + 1) .artist_block'
			);
			var artistBlocksGroup2 = $(
				'.people_artists .column_1_3:nth-child(3n + 2) .artist_block'
			);
			var artistBlocksGroup3 = $(
				'.people_artists .column_1_3:nth-child(3n + 3) .artist_block'
			);

			// Set the page overlay color

			pageOverlay.css({ 'background-color': teal, opacity: '.3' });

			// Fade in the page overlay so we see the content fade out behind a veil

			pageOverlay.fadeIn('slow', function() {
				// Fade out the pages contents

				peopleBorder.velocity({ height: '0%', backgroundColor: '#FFF' }, 1000);
				artistBlocksGroup1.velocity({ marginTop: '-30px', opacity: 0 }, 1000);
				artistBlocksGroup2.velocity({ marginTop: '30px', opacity: 0 }, 1000);
				artistBlocksGroup3.velocity({ marginTop: '-30px', opacity: 0 }, 1000);
				// peopleArtists.velocity({paddingTop: '50px', opacity: 0}, 1000);
				peopleClients.velocity(
					{ paddingTop: '50px', opacity: 0 },
					1000,
					function() {
						// Make the page overlay white while the new content transitions in
						pageOverlay.velocity(
							{ backgroundColor: '#FFF', opacity: 1 },
							500,
							function() {
								// Empty the current content on the page
								ajaxContainer.empty();
								// Load in the new pages content
								ajaxContainer.load(pageURL + ' .main_wrapper', function() {
									// Change the browser URL & document title
									window.history.pushState(pageSlug, pageTitle, pageURL);
									document.title = pageTitle;
									$.ajaxSetup({ cache: true });
									$.getScript(
										wpSiteURL +
											'/wp-content/themes/humbleriot/js/scripts.min.js'
									);
									pageOverlay.velocity(
										{ opacity: 0 },
										{
											duration: 1500,
											complete: function() {
												pageOverlay.css({ display: 'none', height: '100%' });
												if (pageSlug === 'projects' || pageSlug === 'people') {
													siteHeader.addClass('dark');
												} else {
													siteHeader.removeClass('dark');
												}
											},
										}
									);
								});
							}
						);
					}
				);
			});
		};

		/*=======================
		Upon clicking an ajax link on the site transition the page.
		=======================*/

		if (mq.matches) {
			$(document).on(
				'click',
				'a.logo, #menu-item-20 a, #menu-item-19 a, a.grid_item_image_container',
				function(e) {
					e.preventDefault();

					// Grab the url, slug and title of the page we are transitioning to

					var pageURL = $(this).attr('href');

					var pageSlug;

					if (pageURL === wpSiteURL) {
						pageSlug = 'home';
					} else {
						pageSlug = lastUrlSection(pageURL);
					}

					var pageTitle = slugToTitle(pageSlug);

					// Check our current page to execute the proper leaving transition

					var currentPage = $('main').attr('data-page');

					// Check if the current page is the page we are going to. So that a page transition does not occur.

					if (currentPage === pageSlug) {
						var currentPageIsTransitionPage = true;
					}

					if (currentPage === 'home' && !currentPageIsTransitionPage) {
						homeLeaving(pageSlug, pageURL, pageTitle);
					} else if (
						currentPage === 'projects' &&
						!currentPageIsTransitionPage
					) {
						projectsLeaving(pageSlug, pageURL, pageTitle);
					} else if (
						currentPage === 'single-project' &&
						!currentPageIsTransitionPage
					) {
						singleProjectLeaving(pageSlug, pageURL, pageTitle);
					} else if (currentPage === 'people' && !currentPageIsTransitionPage) {
						peopleLeaving(pageSlug, pageURL, pageTitle);
					} else if (currentPageIsTransitionPage) {
					} else {
						window.location.href = pageURL;
					}

					// Change the active menu link

					changeActiveMenuLink(pageSlug);
				}
			);
		}

		/*=======================
		On popstate transition the page
		=======================*/

		if (mq.matches) {
			window.addEventListener('popstate', function(e) {
				// Set the URL we want grab content from

				var pageURL;

				var pageSlug = e.state;

				if (e.state !== 'home') {
					pageURL = wpSiteURL + pageSlug;
				} else {
					pageURL = wpSiteURL;
				}

				var pageTitle = slugToTitle(pageSlug);

				// Check our current page to execute the proper leaving transition

				var currentPage = $('main').attr('data-page');

				switch (currentPage) {
					case 'home':
						homeLeaving(pageSlug, pageURL, pageTitle);
						break;
					case 'projects':
						projectsLeaving(pageSlug, pageURL, pageTitle);
						break;
					case 'single-project':
						singleProjectLeaving(pageSlug, pageURL, pageTitle);
						break;
					case 'people':
						peopleLeaving(pageSlug, pageURL, pageTitle);
						break;
					default:
				}
			});
		}
	});
})(jQuery);
