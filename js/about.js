(function($) {

	$(document).ready(function() {

	"use strict";

		var aboutLink = $('#menu-item-16');
		var aboutSection = $('section.about');
		var aboutBorder = $('.about_bordered_content');
		var aboutLeftContent = $('.about_left_half h1');
		var aboutRightContent = $('.about_right_half p');
		var aboutClose = $('.about .close_icon');
		var mainNavigation = $('.main_navigation');
		var mainHeader = $('.main_header');

		// Colors

		var teal = '#57BFC2';
		var white = '#FFFFFF';

		// Set the changed header variable to be used once the about screen is triggered

		var changedHeaderColor;

		/*================================= 
		Click the 'About' link in the header and reveal the screen.
		=================================*/

		aboutLink.click(function(e){
			e.preventDefault();

			// Check if the header is currently in a dark state.
			// If so then change it to white so the logo is visible.
			if(mainHeader.hasClass('dark')) {
				mainHeader.removeClass('dark');
				changedHeaderColor = true;
			}
			else {
				changedHeaderColor = false;
			}

			// Fade out the navigation
			mainNavigation.fadeOut();

			// Transition in the about section
			aboutSection.fadeIn(500, function(){
				aboutBorder.velocity({opacity: 1}, function(){
					aboutBorder.velocity({height: '90%', borderColor: teal}, 1000, 'easeInCubic', function(){
						aboutLeftContent.velocity({opacity: 1}, 1200);
						aboutRightContent.velocity({opacity: 1, marginTop: '0px'}, 1500);
						aboutClose.velocity({opacity: 1, marginTop: '0px'}, 1500);
					});
				});
			});
		});

		/*================================= 
		Close the 'About' screen
		=================================*/

		aboutClose.click(function(){
			// Fade In the navigation again
			mainNavigation.fadeIn();

			// Transition out the about screen elements
			aboutLeftContent.velocity({opacity: 0}, 1000);
			aboutClose.velocity({opacity: 0, marginTop: '30px'}, 1500);
			aboutRightContent.velocity({opacity: 0, marginTop: '50px'}, 1000, function(){
				aboutBorder.velocity({height: 0, borderColor: white}, 1000, 'easeInCubic', function(){
					aboutBorder.velocity({opacity: 0}, function(){
						aboutSection.fadeOut(1000);
						// If the header changed previously then change it back to its dark state.
						if(changedHeaderColor) {
							mainHeader.addClass('dark');
						};
					});
				});
			});
		});

	});

})(jQuery);