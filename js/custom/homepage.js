(function($) {

	$(document).ready(function() {

	"use strict";

		var mainHeader = $('.main_header');
		var homePanelContainer = $('.humbleriot_panels');
		var homePanelContent = $('.panel_content_container');
		var homePanelText = $('.panel svg');

		/*================================= 
		PAGE LOAD EFFECTS
		=================================*/

		var homeCenterPanel = $('.humbleriot_panels .center');
		var homeLeftPanel = $('.humbleriot_panels .left');
		var homeRightPanel = $('.humbleriot_panels .right');

		window.setTimeout(function(){
			homeCenterPanel.velocity({paddingTop: '0px', opacity: 1}, 1000);
			homeLeftPanel.velocity({paddingTop: '0px', opacity: 1}, 1000);
			homeRightPanel.velocity({paddingTop: '0px', opacity: 1}, 1000);
		}, 800);

		/*==================================
		Homepage Panel Hover Effects
		==================================*/

		// Hover into a panel and reveal its description/background image.

		var homePanelHoverIn = function() {
			var panelName = $(this).attr('data-name');
			$('.panel_background_images #' + panelName).velocity({ opacity: 1 }, 700);
			homePanelContainer.addClass(panelName);
		};

		// Hover out of a panel and hide its description/background image.

		var homePanelHoverOut = function() {
			var panelName = $(this).attr('data-name');
			$('.panel_background_images #' + panelName).velocity({ opacity: 0 }, 700);
			homePanelContainer.removeClass(panelName);
		};

		homePanelContent.hover(homePanelHoverIn, homePanelHoverOut);

	});

})(jQuery);