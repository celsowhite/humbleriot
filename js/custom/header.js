(function($) {

	$(document).ready(function() {

	"use strict";

		var mobileNavigation = $('.mobile_navigation');
		var mobileMenuIcon = $('.mobile_header .menu_icon');
		var mobileCloseIcon = $('.mobile_navigation .close_icon');

		/*================================= 
		MOBILE NAVIGATION REVEAL
		=================================*/
		
		mobileMenuIcon.click(function(){
			mobileNavigation.fadeIn('slow');
		});

		mobileCloseIcon.click(function(){
			mobileNavigation.fadeOut('slow');
		});

	});

})(jQuery);

