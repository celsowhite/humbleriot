(function($) {
	
	"use strict";
	
	$(document).ready(function() {

		var headerImage = $('.single_project_header');
		var headerImageHeight;
		var headerImageOverlay = $('.single_project_header_overlay');
		var projectContainer = $('.single-project .container');
		var videoContainer = $('.project_video_container');
		var videoCloseContainer = $('.project_video');
		var playButton = $('.play_button');
		var closeVideo = $('.project_video_container .close_icon');
		var mainHeader = $('.main_header');

		/*================================= 
		PAGE LOAD EFFECTS
		=================================*/

		window.setTimeout(function(){

			// Return header height to normal

			headerImage.velocity({height: '70vh'}, 1000, 'easeOutQuart', function(){

				// Capture the header image height so we can use it to trigger the nav color change.
				
				headerImageHeight = headerImage.height();
			});

			// Remove line over header image and then fade in the page content.

			headerImageOverlay.velocity({backgroundColorAlpha: 0, height: 0}, 1000, function(){
				projectContainer.velocity({opacity: 1}, {duration: 500, easing: 'easeInCubic'});
				playButton.velocity({opacity: 1}, 500);
			});
		}, 2000);

		/*================================= 
		PLYR Video Setup and Control
		=================================*/

		// Setup plyr to find <video> elements on the page.

		var players = plyr.setup({
			controls: ['play', 'fullscreen', 'progress']
		});

		// Click play button and play

		playButton.click(function(){

			videoContainer.fadeIn('slow');

			// Autoplay video on desktop.
			
			if(mq.matches) {
				players[0].play();
			}

		});

		// Close and pause

		closeVideo.click(function(){
			players[0].pause();
			videoContainer.fadeOut('slow');
		});

		/*================================= 
		SCROLL EFFECTS TO HEADER
		=================================*/

		// Only on single project pages change the header color after scrolling past the header image.
		// Header image is dark and the page is white so need header to be visible past a certain point.

		$(window).scroll(function(){
			if($('.main_wrapper').hasClass('single-project')) {
				var scrollTop = $(window).scrollTop();
				if(scrollTop >= headerImageHeight) {
					mainHeader.addClass('dark');
				}
				else {
					mainHeader.removeClass('dark');
				}
			}
		});

	});

})(jQuery);