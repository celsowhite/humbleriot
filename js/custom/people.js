(function($) {

	$(document).ready(function() {

	"use strict";

		var peopleBorder = $('.people_border');
		var peopleArtists = $('.people_artists');
		var peopleClients = $('.people_clients');
		var artistBlocks = $('.artist_block');

		/*================================= 
		PAGE LOAD EFFECTS
		=================================*/

		window.setTimeout(function(){
			peopleBorder.velocity({height: '100%', backgroundColor: '#000'}, 1000);
			artistBlocks.velocity({marginTop: 0, opacity: 1}, 1000);
			peopleClients.velocity({paddingTop: 0, opacity: 1}, 1000);
		}, 800);

	});

})(jQuery);

