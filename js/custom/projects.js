(function($) {

	$(document).ready(function() {

	"use strict";

		var projectsGridItems = $('.grid_item');
		var projectGridItemInner = $('.grid_item:nth-child(3n+1)');
		var projectGridItemMiddle = $('.grid_item:nth-child(3n)');
		var projectGridItemOuter = $('.grid_item:nth-child(3n+2)');

		/*================================= 
		PAGE LOAD EFFECTS
		=================================*/

		window.setTimeout(function(){
			projectsGridItems.velocity({opacity: 1, paddingTop: '0px'}, 1000);
		}, 500);

		/*================================= 
		GRID ITEM PARALLAX
		=================================*/

		if(mq.matches) {
			$(window).scroll(function(){
				var offsetTopInner = -($(window).scrollTop()/3);
				var offsetTopMiddle = ($(window).scrollTop()/5);
				var offsetTopOuter = -($(window).scrollTop()/5);
				projectGridItemInner.css({
					'webkit-transform':'translateY(' + offsetTopInner + 'px)'
				});
				projectGridItemMiddle.css({
					'webkit-transform':'translateY(' + offsetTopMiddle + 'px)'
				});
				projectGridItemOuter.css({
					'webkit-transform':'translateY(' + offsetTopOuter + 'px)'
				});
			});
		}

	});

})(jQuery);

