(function($) {

	$(document).ready(function() {

	"use strict";

		// Contact

		var connectSection = $('section.connect');
		var connectContent = $('.connect_page_container');
		var connectEmailLine = $('.connect_email_line');
		var connectClose = $('section.connect .close_icon');
		var pageOverlay = $('.page_opacity_overlay');
		var socialIcons = $('ul.social_icons');

		$('#menu-item-17').click(function(e){
			e.preventDefault();
			connectSection.velocity({width: '50%'}, 1000);
			connectEmailLine.velocity({width: '100%'}, { delay: 300, duration: 800 });
			socialIcons.velocity({opacity: 1, marginRight: '0px'}, { delay: 300, duration: 800 });
			pageOverlay.css({'background-color':'#57BFC2', 'opacity': '.5'});
			pageOverlay.delay(500).fadeIn('slow');
		});

		connectClose.click(function(e){
			socialIcons.velocity({opacity: 0, marginRight: '-50px'}, 800);
			connectEmailLine.velocity({width: '0%'}, 800, function(){
				connectSection.velocity({width: 0}, 'easeInCubic', 800);
				pageOverlay.fadeOut();
			});
		});

	});

})(jQuery);