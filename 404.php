<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package _s
 */

get_header(); ?>

	<main id="main" class="main_wrapper" role="main">

	</main>

<?php get_footer(); ?>
