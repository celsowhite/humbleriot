<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package _s
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<!-- Favicon -->

<link rel="icon" type="img/png" href="<?php echo get_template_directory_uri() . '/favicon.png'; ?>" />

<!-- Save base wordpress URL for usage in ajax -->

<script type="text/javascript">
	var wpSiteURL = '<?php echo esc_url( home_url( '' ) ); ?>';
	var mq = window.matchMedia( '(min-width: 1024px)' );
</script>

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

	<!-- Main Header -->

	<header class="main_header <?php if(is_page_template('page-templates/projects.php') || is_page_template('page-templates/people.php') || is_404()): ?>dark<?php endif; ?>">
		<a class="logo" href="<?php echo esc_url( home_url( '/' ) ); ?>">
			<img class="logo_white" src="<?php echo get_template_directory_uri() . '/img/logo_white.png'; ?>" />
			<img class="logo_black" src="<?php echo get_template_directory_uri() . '/img/logo_black.png'; ?>" />
		</a>
		<nav class="main_navigation">
			<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_id' => 'primary-menu', 'container' => '' ) ); ?>
		</nav>
	</header>

	<!-- Mobile Header -->

	<header class="mobile_header white">
		<a class="logo" href="<?php echo esc_url( home_url( '/' ) ); ?>">
			<img class="logo_black" src="<?php echo get_template_directory_uri() . '/img/logo_black.png'; ?>" />
		</a>
		<span class="menu_icon"></span>
	</header>

	 <!-- Mobile Navigation -->

    <?php get_template_part('template-parts/mobile_navigation'); ?>

    <!-- About Flyout -->

	<?php get_template_part('template-parts/about_content'); ?>

	<!-- Connect Flyout -->

	<?php get_template_part('template-parts/connect_content'); ?>

	<div class="page_opacity_overlay"></div>

	<div id="content" class="main_content">