<section class="connect">
	<div class="connect_page_container">
		<h1>
			<a class="connect_email" href="mailto:<?php the_field('humbleriot_email', 13); ?>"><?php the_field('humbleriot_email', 13); ?> <div class="connect_email_line"></div></a>
		</h1>
		<ul class="social_icons">
			<li><a href="<?php the_field('humbleriot_twitter', 13); ?>" target="_blank"><i class="fa fa-twitter"></i></a></li>
			<li><a href="<?php the_field('humbleriot_instagram', 13); ?>" target="_blank"><i class="fa fa-instagram"></i></a></li>
			<li><a href="<?php the_field('humbleriot_facebook', 13); ?>" target="_blank"><i class="fa fa-facebook"></i></a></li>
			<li><a href="<?php the_field('humbleriot_soundcloud', 13); ?>" target="_blank"><i class="fa fa-soundcloud"></i></a></li>
		</ul>
		<span class="close_icon"></span>
	</div>
</section>