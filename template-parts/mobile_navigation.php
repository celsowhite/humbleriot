<div class="mobile_navigation">
    <div class="content_container">
    	<header class="mobile_header black">
        	<a class="logo" href="<?php echo esc_url( home_url( '/' ) ); ?>">
				<img class="logo_white" src="<?php echo get_template_directory_uri() . '/img/logo_white.png'; ?>" />
			</a>
			<div class="close_icon"></div>
		</header>
        <?php wp_nav_menu( array( 'menu' => 'mobile-menu', 'menu_id' => 'mobile-menu', 'container' => '' ) ); ?>
    	<div class="mobile_navigation_connect">
    		<a href="<?php the_field('humbleriot_email', 13); ?>"><?php the_field('humbleriot_email', 13); ?></a>
    		<ul class="social_icons">
				<li><a href="<?php the_field('humbleriot_twitter', 13); ?>" target="_blank"><i class="fa fa-twitter"></i></a></li>
				<li><a href="<?php the_field('humbleriot_instagram', 13); ?>" target="_blank"><i class="fa fa-instagram"></i></a></li>
				<li><a href="<?php the_field('humbleriot_facebook', 13); ?>" target="_blank"><i class="fa fa-facebook"></i></a></li>
				<li><a href="<?php the_field('humbleriot_soundcloud', 13); ?>" target="_blank"><i class="fa fa-soundcloud"></i></a></li>
			</ul>
    	</div>
    </div>
</div>