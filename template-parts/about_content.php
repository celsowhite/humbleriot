<section class="about">
	<div class="about_page_container">
		<div class="about_bordered_content">
			<div class="about_content_container">
				<div class="humbleriot_row">
					<div class="column_1_2 about_left_half">
						<h1><?php the_field('about_left_column', 7); ?></h1>
					</div>
					<div class="column_1_2 about_right_half">
						<p><?php the_field('about_right_column', 7); ?></p>
					</div>
				</div>
			</div>
			<span class="close_icon"></span>
		</div>
	</div>
</section>
