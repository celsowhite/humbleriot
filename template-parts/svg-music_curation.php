<svg width="213px" height="343px" viewBox="0 0 213 343" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
    <!-- Generator: Sketch 41.2 (35397) - http://www.bohemiancoding.com/sketch -->
    <title>Music Curation</title>
    <desc>Created with Sketch.</desc>
    <defs></defs>
    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" font-weight="normal" font-family="league_gothic" letter-spacing="1.39999998" font-size="120" fill-opacity="0">
        <g id="Music-Curation" stroke="#FFFFFF" fill="#4A4A4A">
            <text id="MUSIC" transform="translate(38.500000, 174.230469) rotate(-90.000000) translate(-38.500000, -174.230469) ">
                <tspan x="-75.7304688" y="225.730469">MUSI</tspan>
                <tspan x="95.5895312" y="225.730469">C</tspan>
            </text>
            <text id="CURATION" transform="translate(173.500000, 174.730469) rotate(-270.000000) translate(-173.500000, -174.730469) ">
                <tspan x="4.76953125" y="226.230469">CURATION</tspan>
            </text>
        </g>
    </g>
</svg>