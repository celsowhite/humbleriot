<svg width="213px" height="261px" viewBox="0 0 213 261" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
    <!-- Generator: Sketch 41.2 (35397) - http://www.bohemiancoding.com/sketch -->
    <title>Social Impact</title>
    <desc>Created with Sketch.</desc>
    <defs></defs>
    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" font-weight="normal" font-family="league_gothic" letter-spacing="1.39999998" font-size="120" fill-opacity="0">
        <g id="Social-Impact" stroke="#FFFFFF" fill="#4A4A4A">
            <text id="SOCIAL" transform="translate(39.500000, 120.845703) rotate(-90.000000) translate(-39.500000, -120.845703) ">
                <tspan x="-85.1152344" y="172.345703">SOCIAL</tspan>
            </text>
            <text id="IMPACT" transform="translate(174.500000, 170.730469) rotate(-270.000000) translate(-174.500000, -170.730469) ">
                <tspan x="5.76953125" y="222.230469">IMPACT</tspan>
            </text>
        </g>
    </g>
</svg>