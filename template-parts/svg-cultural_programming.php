<svg width="508px" height="216px" viewBox="0 0 508 216" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
    <!-- Generator: Sketch 41.2 (35397) - http://www.bohemiancoding.com/sketch -->
    <title>Cultural Programming</title>
    <desc>Created with Sketch.</desc>
    <defs></defs>
    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" font-weight="normal" font-family="league_gothic" letter-spacing="1.39999998" font-size="120" fill-opacity="0">
        <g id="Cultural-Programming" stroke="#FFFFFF" fill="#4A4A4A">
            <text id="CULTURAL" transform="translate(256.730469, 40.500000) rotate(-360.000000) translate(-256.730469, -40.500000) ">
                <tspan x="87" y="92">CULTURAL</tspan>
            </text>
            <text id="PROGRAMMING" transform="translate(253.230469, 175.500000) rotate(-180.000000) translate(-253.230469, -175.500000) ">
                <tspan x="5.68434189e-14" y="227">PROGRAMMING</tspan>
            </text>
        </g>
    </g>
</svg>