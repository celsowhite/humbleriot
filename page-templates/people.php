<?php
/*
Template Name: People
*/

get_header(); ?>

	<?php while ( have_posts() ) : the_post(); ?>

		<main class="main_wrapper people" data-page="people">

			<div class="container">

				<!-- Artists -->

				<div class="people_artists">
					<div class="humbleriot_row">
						<?php if( have_rows('humbleriot_artists') ): while ( have_rows('humbleriot_artists') ) : the_row(); ?>
							<div class="column_1_3">
								<div class="artist_block">
									<img src="<?php the_sub_field('artist_image'); ?>" />
									<div class="artist_name">
										<span><?php the_sub_field('artist_name'); ?></span>
									</div>
								</div>
							</div>
			    		<?php endwhile; endif; ?>
					</div>
				</div>

				<!-- Border -->

				<span class="people_border"></span>

				<!-- Clients -->

				<ul class="people_clients">
					<?php if( have_rows('humbleriot_clients') ): while ( have_rows('humbleriot_clients') ) : the_row(); ?>
						<li><?php the_sub_field('client_name'); ?></li>
		    		<?php endwhile; endif; ?>
				</ul >
			</div>

			<!-- Bottom Gradient -->

			<div class="bottom_gradient"></div>

		</main>

	<?php endwhile; ?>

<?php get_footer(); ?>