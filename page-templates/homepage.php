<?php
/*
Template Name: Homepage
*/

get_header(); ?>

	<?php while ( have_posts() ) : the_post(); ?>

		<main class="main_wrapper home" data-page="home" style="background-image: url('<?php the_post_thumbnail_url(); ?>');">

			<!-- Background Images -->

			<div class="panel_background_images">
				<div id="music_curation" style="background-image: url('<?php the_field('music_curation_background_image'); ?>');"></div>
				<div id="cultural_programming" style="background-image: url('<?php the_field('cultural_programming_background_image'); ?>');"></div>
				<div id="social_impact" style="background-image: url('<?php the_field('social_impact_background_image'); ?>');"></div>
			</div>

			<!-- Panels -->

			<div class="humbleriot_panels">
				<section class="panel left">
					<div class="panel_content_container" data-name="music_curation">
						<?php echo get_template_part('template-parts/svg', 'music_curation'); ?>
						<p class="content"><?php the_field('music_curation_description'); ?></p>
					</div>
				</section>

				<section class="panel center">
					<div class="panel_content_container" data-name="cultural_programming">
						<?php echo get_template_part('template-parts/svg', 'cultural_programming'); ?>
						<p class="content"><?php the_field('cultural_programming_description'); ?></p>
					</div>
				</section>

				<section class="panel right">
					<div class="panel_content_container" data-name="social_impact">
						<?php echo get_template_part('template-parts/svg', 'social_impact'); ?>
						<p class="content"><?php the_field('social_impact_description'); ?></p>
					</div>
				</section>
			</div>

			<!-- Mobile Homepage -->

			<div class="mobile_homepage">
				<section class="mobile_homepage_panel">
					<h1>Music Curation</h1>
					<p><?php the_field('music_curation_description'); ?></p>
				</section>
				<section class="mobile_homepage_panel">
					<h1>Cultural Programming</h1>
					<p><?php the_field('cultural_programming_description'); ?></p>
				</section>
				<section class="mobile_homepage_panel">
					<h1>Social Impact</h1>
					<p><?php the_field('social_impact_description'); ?></p>
				</section>
			</div>

		</main>

	<?php endwhile; ?>

<?php get_footer(); ?>
