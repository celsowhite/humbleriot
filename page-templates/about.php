<?php
/*
Template Name: About
*/

get_header(); ?>

	<?php while ( have_posts() ) : the_post(); ?>

		<main class="main_wrapper about_page">
			<?php get_template_part('template-parts/about_content'); ?>
		</main>

	<?php endwhile; ?>

<?php get_footer(); ?>