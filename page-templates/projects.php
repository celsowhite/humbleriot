<?php
/*
Template Name: Projects
*/

get_header(); ?>

	<?php while ( have_posts() ) : the_post(); ?>

		<main class="main_wrapper projects" data-page="projects">

			<!-- Projects Grid -->

			<div class="humbleriot_projects_grid">

				<?php
					$projects_args = array('post_type' => 'projects', 'posts_per_page' => -1, 'order' => 'ASC');
					$projects_loop = new WP_Query($projects_args);
					if ( $projects_loop->have_posts() ) : while ( $projects_loop->have_posts() ) : $projects_loop->the_post();
				?>	
					<div class="grid_item">

						<a href="<?php the_permalink(); ?>" class="grid_item_image_container">
							<?php the_post_thumbnail(); ?>
							<div class="grid_item_overlay"></div>
							<h2><?php the_title(); ?></h2>
						</a>

					</div>

				<?php endwhile; wp_reset_postdata(); endif; ?>

			</div>

			<!-- Bottom Gradient -->

			<div class="bottom_gradient"></div>

		</main>

	<?php endwhile; ?>

<?php get_footer(); ?>